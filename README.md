# smartiot
The project is about distributed home Automation for controlling and monitoring various devices with Android app based on IoT.
Requirements

Hardware :1. NodeMCU v1.0 also known as ESP8266-12E
          2. Sensors DHT11, RFID reader - card, number keypad 4x4
Software :	Arduino IDE
			Android IDE

We used PHP-mysql for maintenance of database. Android- volley framework to build <a href="https://gitlab.com/arunteja212/iot_home_automation.git">android app</a>.

# Abstract
The advent of Internet of Things, made Internet to get deep rooted into every technological innovation to be smart, intelligent, and easy accessibility. Likewise are the home automation devices. Conventional home automation is based on centralised control. Such systems suffer portability, flexibility and extensibility. To overcome the drawbacks, a distributed control in home automation with IoT is proposed. The proposal is visualized by means of client server architecture using LAMP server setup on a raspberry pi, an android app and arduino, NodeMCUs as clients. MFRC522 RFID reader, keypad, DHT-11 sensor, are at the physical level. The proposal is implemented for monitoring and controlling devices with a client-server architecture in the home and also maintain data logs for future needs.

# Architectural design
![final_architecture](/uploads/7a78d7e1b01f807c7006d78b5d773eac/final_architecture.png)

# Implementation Logic for each application Component
1.Temperature-Humidity client flow digaram\
![Temp_Humi_client](/uploads/07074d6a6f5827b48f40bcaafd9171b7/Temp_Humi_client.png)


2. Switching on/off client flow diagram\
![Switch_client](/uploads/7315b396c0711f751331b84c6c338e68/Switch_client.png)


3. RFID client flow diagram\
![RFID_client](/uploads/a3cafac36622148d0dd77ecde759ac72/RFID_client.png)


4. Locker access client flow diagram\
![Keypad_client](/uploads/95d230d5e6c88112c6dcf01874db4b60/Keypad_client.png)









